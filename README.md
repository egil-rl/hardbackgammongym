# Backgammon Hard Gym

# Table of Contents
1. [Quick Start](#quick-start)
2. [Introduction](#introduction)
3. [Game Engine](#game-engine)
4. [Feature Space](#feature-space)
5. [Configuring Your Agent](#configuring-your-agent)
6. [Technology Used](#technology-used)

## Quick Start
To run the project make sure to install the related python packages with pip. Then run the code. This should be as simple as...

```
pip install -r requirements.txt
python3 main.py
```

## Introduction
Backgammon Hard Gym is a reinforcement learning environment modeled on Teseuvos' TD-gammon game engine. It's designed to offer a more complex backgammon experience, with all 24 markers on the board and a split between WHITE and BLACK checkers ranging from 7 to 18. The game adheres to official backgammon rules, requiring all checkers to be in the bearoff position before they can be removed. The aim is for agents to bear off their 15 checkers first, with a randomized starting position for each game.

## Game Engine
The engine manages the game by manipulating an array of numbers to represent the board state, moving checkers, and determining the winner. Its output includes legal moves as board states and the identification of a winning player. The environment's goal is to train two RL agents to choose optimal moves based on the initial board state for their respective players (black or white).

## Feature Space
The board state includes 29 elements:
- **White Player's Board Configuration (Elements 0-[6-17])**: Marks the positions of WHITE player's checkers during bearoff. Index 0 is the furthest from bearing off, and 11 is the closest.
- **Black Player's Board Configuration (Elements [7-18]-23)**: Similar to the WHITE player's configuration but occupies the remaining part of the board.
- **Checkers Off the Board (Elements 24-25)**: Element 24 for WHITE and 25 for BLACK checkers.
- **Current Player's Turn (Elements 26-27)**: Element 26 for WHITE's turn and 27 for BLACK's.
- **Split Point (Element 28)**: Indicates the division between WHITE and BLACK checkers on the board, with a value between 6 and 17.

## Configuring Your Agent

### Example: Two Random Agents
`main.py` demonstrates the use of two random agents, showing how to play the game and integrate reinforcement learning agents for training or competition. The focus here is on self-learning.

### Board Initialization and Legal Moves
- Initialize the board with the constructor, which allows iterating through legal moves using the `moves` attribute.
- Agents can be provided a static board state for initialization to learn from multiple games with the same initial state.

### Agent's Responsibilities
- Provide a scalar value between 0 and 1 for each move using the `score_move` function, reflecting the move's suitability for their respective player.
- Check the game's winner after each turn with `get_winner`.

## Technology Used
- **Python**: The primary language used for developing the game engine and scripting the reinforcement learning environment.

**Note**: This RL environment is designed for experimental and educational purposes in advanced game strategy and reinforcement learning.
